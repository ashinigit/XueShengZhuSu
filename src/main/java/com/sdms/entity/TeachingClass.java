package com.sdms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.persistence.*;
import java.util.Set;

/**
 * 教学班级
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "t_class")
@Schema(title = "学生的班级")
public class TeachingClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, columnDefinition = "bigint(20) comment '教学班级的主键'")
    @Schema(title = "主键")
    private Long id;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '教学班级的名称'")
    @Schema(title = "名称")
    private String name;

    // 一个班级有多个学生
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "teachingClass")
    @JsonIgnore
    private Set<Student> students;

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Set<Student> getStudents() {
        return this.students;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @JsonIgnore
    public void setStudents(final Set<Student> students) {
        this.students = students;
    }
}
