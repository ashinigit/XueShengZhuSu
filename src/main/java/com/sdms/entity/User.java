package com.sdms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.persistence.*;

/**
 * 用户
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "t_user")
@Schema(title = "用户")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, columnDefinition = "bigint(20) comment '用户的主键'")
    @Schema(title = "主键")
    private Long id;

    @Column(nullable = false, unique = true, columnDefinition = "varchar(255) comment '用于登录的账号'")
    @Schema(title = "账号,即用户名")
    private String username;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '数据库中存储的密码'")
    @Schema(title = "密码")
    @JsonIgnore // 密码不应该在前台展示
    private String password;

    @Column(columnDefinition = "varchar(255) comment '用户的头像,数据库中仅存储文件路径'")
    @Schema(title = "头像")
    private String avatar;

    @Column(columnDefinition = "varchar(255) comment '用户的联系方式'")
    @Schema(title = "联系方式")
    private String phone;

    @Column(columnDefinition = "varchar(255) comment '用户的地址'")
    @Schema(title = "地址")
    private String address;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "varchar(255) default 'MALE' comment '用户的性别'")
    @Schema(title = "性别")
    @JsonIgnore
    private Gender gender;

    /**
     * 每个用户有自己的角色,多个用户可以有同一个角色
     * CascadeType见 <a href="https://www.jianshu.com/p/e8caafce5445">参考文档</a>
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    @Schema(title = "角色")
    @JsonIgnore
    private Role role;
    
    public enum Gender {

        MALE("男"),
        FEMALE("女");

        private final String value;

        Gender(String value) {
            this.value = value;
        }

        public static Gender of(String genderStr) {
            for (Gender gender : values()) {
                if (gender.value.equals(genderStr)) {
                    return gender;
                }
            }
            return null;
        }

        public String getValue() {
            return this.value;
        }
    }

    @Transient
    @Schema(title = "临时字段:角色名称")
    private String roleName;

    @Transient
    @Schema(title = "临时字段:角色id")
    private Long roleId;

    @Transient
    @Schema(title = "临时字段:用字符串描述的性别")
    private String genderStr;

    public Long getId() {
        return this.id;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public String getPhone() {
        return this.phone;
    }

    public String getAddress() {
        return this.address;
    }

    public Gender getGender() {
        return this.gender;
    }

    public Role getRole() {
        return this.role;
    }

    public String getRoleName() {
        return this.roleName;
    }

    public Long getRoleId() {
        return this.roleId;
    }

    public String getGenderStr() {
        return this.genderStr;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    @JsonIgnore
    public void setPassword(final String password) {
        this.password = password;
    }

    public void setAvatar(final String avatar) {
        this.avatar = avatar;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    @JsonIgnore
    public void setGender(final Gender gender) {
        this.gender = gender;
    }

    @JsonIgnore
    public void setRole(final Role role) {
        this.role = role;
    }

    public void setRoleName(final String roleName) {
        this.roleName = roleName;
    }

    public void setRoleId(final Long roleId) {
        this.roleId = roleId;
    }

    public void setGenderStr(final String genderStr) {
        this.genderStr = genderStr;
    }
}