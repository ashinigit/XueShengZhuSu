package com.sdms.entity;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.util.Set;

/**
 * 寝室
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "t_room")
@Schema(title = "寝室")
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, columnDefinition = "bigint(20) comment '寝室的主键'")
    @Schema(title = "主键")
    private Long id;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '寝室的名称'")
    @Schema(title = "名称")
    private String name;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '寝室的照片'")
    @Schema(title = "照片")
    private String picture;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '寝室的地址'")
    @Schema(title = "地址")
    private String address;

    //每个寝室都有自己的类型,不同的寝室可以是同一种类型
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    //一个寝室中居住有多名学生
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "room")
    private Set<Student> students;

    @Transient
    @Schema(title = "临时字段:寝室类型id")
    private Long categoryId;

    @Transient
    @Schema(title = "临时字段:寝室类型名称")
    private String categoryName;

    @Transient
    @Schema(title = "临时字段:住在该寝室的所有学生的姓名和学号,格式为 姓名1|学号1,姓名2|学号2,...")
    private String studentsNameAndId;

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPicture() {
        return this.picture;
    }

    public String getAddress() {
        return this.address;
    }

    public Category getCategory() {
        return this.category;
    }

    public Set<Student> getStudents() {
        return this.students;
    }

    public Long getCategoryId() {
        return this.categoryId;
    }

    public String getCategoryName() {
        return this.categoryName;
    }

    public String getStudentsNameAndId() {
        return this.studentsNameAndId;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setPicture(final String picture) {
        this.picture = picture;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public void setCategory(final Category category) {
        this.category = category;
    }

    public void setStudents(final Set<Student> students) {
        this.students = students;
    }

    public void setCategoryId(final Long categoryId) {
        this.categoryId = categoryId;
    }

    public void setCategoryName(final String categoryName) {
        this.categoryName = categoryName;
    }

    public void setStudentsNameAndId(final String studentsNameAndId) {
        this.studentsNameAndId = studentsNameAndId;
    }
}
