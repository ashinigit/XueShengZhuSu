package com.sdms.entity;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.persistence.*;

/**
 * 权限，每种角色都有对应可执行操作的权限，未完成...
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "t_permission")
@Schema(title = "权限")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, columnDefinition = "bigint(20) comment '权限的主键'")
    @Schema(title = "主键")
    private Long id;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限的名称'")
    @Schema(title = "名称")
    private String name;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限的描述'")
    @Schema(title = "描述")
    private String description;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '权限码'")
    @Schema(title = "权限码")
    private String code;

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public String getCode() {
        return this.code;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setCode(final String code) {
        this.code = code;
    }
}
