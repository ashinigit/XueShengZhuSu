package com.sdms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.util.Set;

/**
 * 寝室类型
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "t_category")
@Schema(title = "寝室类型")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, columnDefinition = "bigint(20) comment '寝室类型的主键'")
    @Schema(title = "主键")
    private Long id;

    @Column(nullable = false, columnDefinition = "varchar(255) comment '寝室类型的名称'")
    @Schema(title = "名称")
    private String name;

    @Column(nullable = false, columnDefinition = "bigint(20) comment '寝室可容纳学生人数的最大值'")
    @Schema(title = "可容纳学生人数的最大值")
    private Long maxSize;

    //一个寝室类型对应的多个寝室
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "category")
    @JsonIgnore
    private Set<Room> rooms;

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Long getMaxSize() {
        return this.maxSize;
    }

    public Set<Room> getRooms() {
        return this.rooms;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setMaxSize(final Long maxSize) {
        this.maxSize = maxSize;
    }

    @JsonIgnore
    public void setRooms(final Set<Room> rooms) {
        this.rooms = rooms;
    }
}
