package com.sdms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.persistence.*;

/**
 * 学生
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "t_student")
@Schema(title = "学生")
public class Student {

    @Id
    @Column(nullable = false, columnDefinition = "varchar(255) comment '学号,即学生的主键'")
    @Schema(title = "学号,即主键")
    private String id; // 学号用字符串表示

    @Column(nullable = false, columnDefinition = "varchar(255) comment '学生的姓名'")
    @Schema(title = "姓名")
    private String name;

    // 学生对应的用户
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    private User user;

    // 每个学生有自己的班级,多个学生可以有同一个班级
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "teaching_class_id", referencedColumnName = "id")
    @JsonIgnore
    private TeachingClass teachingClass;

    // 每个学生有自己的寝室,多个学生可以有同一个寝室
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    @JsonIgnore
    private Room room;

    @Transient
    @Schema(title = "临时字段:用户id")
    private Long userId;

    @Transient
    @Schema(title = "临时字段:账号")
    private String username;

    @Transient
    @Schema(title = "临时字段:班级id")
    private Long teachingClassId;

    @Transient
    @Schema(title = "临时字段:班级名称")
    private String teachingClassName;

    @Transient
    @Schema(title = "临时字段:寝室id")
    private Long roomId;

    @Transient
    @Schema(title = "临时字段:寝室名称")
    private String roomName;

    @Transient
    @Schema(title = "临时字段:联系方式")
    private String phone;

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public User getUser() {
        return this.user;
    }

    public TeachingClass getTeachingClass() {
        return this.teachingClass;
    }

    public Room getRoom() {
        return this.room;
    }

    public Long getUserId() {
        return this.userId;
    }

    public String getUsername() {
        return this.username;
    }

    public Long getTeachingClassId() {
        return this.teachingClassId;
    }

    public String getTeachingClassName() {
        return this.teachingClassName;
    }

    public Long getRoomId() {
        return this.roomId;
    }

    public String getRoomName() {
        return this.roomName;
    }

    public String getPhone() {
        return this.phone;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @JsonIgnore
    public void setUser(final User user) {
        this.user = user;
    }

    @JsonIgnore
    public void setTeachingClass(final TeachingClass teachingClass) {
        this.teachingClass = teachingClass;
    }

    @JsonIgnore
    public void setRoom(final Room room) {
        this.room = room;
    }

    public void setUserId(final Long userId) {
        this.userId = userId;
    }

    public void setUsername(final String username) {
        this.username = username;
    }

    public void setTeachingClassId(final Long teachingClassId) {
        this.teachingClassId = teachingClassId;
    }

    public void setTeachingClassName(final String teachingClassName) {
        this.teachingClassName = teachingClassName;
    }

    public void setRoomId(final Long roomId) {
        this.roomId = roomId;
    }

    public void setRoomName(final String roomName) {
        this.roomName = roomName;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }
}