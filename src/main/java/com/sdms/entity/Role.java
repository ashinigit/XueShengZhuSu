package com.sdms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.*;
import java.util.Set;

/**
 * 角色
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "t_role")
@Schema(title = "角色")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, columnDefinition = "bigint(20) comment '角色的主键'")
    @Schema(title = "主键")
    private Long id;

    @Column(nullable = false, unique = true, columnDefinition = "varchar(255) comment '角色的名称'")
    @Schema(title = "名称")
    private String name;

    // 一个角色可以被多个用户使用
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "role")
    @JsonIgnore
    private Set<User> users;

    // 角色和权限之间的关系是多对多
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "t_role_permission",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id", referencedColumnName = "id"))
    @JsonIgnore
    private Set<Permission> permissions;

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public Set<User> getUsers() {
        return this.users;
    }

    public Set<Permission> getPermissions() {
        return this.permissions;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @JsonIgnore
    public void setUsers(final Set<User> users) {
        this.users = users;
    }

    @JsonIgnore
    public void setPermissions(final Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public static final Long STUDENT_ROLE_ID = 1L;
    public static final Long ADMIN_ROLE_ID = 2L;
}
