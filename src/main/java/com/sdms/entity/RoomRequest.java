package com.sdms.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import javax.persistence.*;

/**
 * 住宿申请
 */
@SuppressWarnings("unused")
@Entity
@Table(name = "t_room_request")
@Schema(title = "住宿申请")
public class RoomRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, columnDefinition = "bigint(20) comment '住宿申请的主键'")
    @Schema(title = "主键")
    private Long id;

    /**
     * 住宿申请的状态包括三种: "未处理"  "成功"  "被拒绝,失败"
     * 这里用枚举表示,数据库中存储字符串
     */
    @Enumerated(EnumType.STRING)
    @Column(nullable = false, columnDefinition = "varchar(255) default 'NO_HANDLE' comment '住宿申请的状态'")
    @JsonIgnore
    private RoomRequestStatus status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "student_id", referencedColumnName = "id")
    @JsonIgnore
    private Student student;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "room_id", referencedColumnName = "id")
    @JsonIgnore
    private Room room;
    
    public enum RoomRequestStatus {

        NO_HANDLE(0, "未处理"),
        SUCCESS(1, "成功分配宿舍"),
        FAILURE(2, "拒绝分配宿舍");

        private final int code;

        private final String message;

        RoomRequestStatus(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public static RoomRequestStatus valueOfCode(int code) {
            for (RoomRequestStatus value : values()) {
                if (value.code == code) {
                    return value;
                }
            }
            return null;
        }
        
        public int getCode() {
            return this.code;
        }
        
        public String getMessage() {
            return this.message;
        }
    }

    @Transient
    @Schema(title = "临时字段:处理状态编码")
    private int statusCode;

    @Transient
    @Schema(title = "临时字段:处理状态描述")
    private String statusMsg;

    @Transient
    @Schema(title = "临时字段:学生id,即学号")
    private String studentId;

    @Transient
    @Schema(title = "临时字段:学生姓名")
    private String studentName;

    @Transient
    @Schema(title = "临时字段:寝室id")
    private Long roomId;

    @Transient
    @Schema(title = "临时字段:寝室名称")
    private String roomName;

    public Long getId() {
        return this.id;
    }

    public RoomRequestStatus getStatus() {
        return this.status;
    }

    public Student getStudent() {
        return this.student;
    }

    public Room getRoom() {
        return this.room;
    }

    public int getStatusCode() {
        return this.statusCode;
    }

    public String getStatusMsg() {
        return this.statusMsg;
    }

    public String getStudentId() {
        return this.studentId;
    }

    public String getStudentName() {
        return this.studentName;
    }

    public Long getRoomId() {
        return this.roomId;
    }

    public String getRoomName() {
        return this.roomName;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @JsonIgnore
    public void setStatus(final RoomRequestStatus status) {
        this.status = status;
    }

    @JsonIgnore
    public void setStudent(final Student student) {
        this.student = student;
    }

    @JsonIgnore
    public void setRoom(final Room room) {
        this.room = room;
    }

    public void setStatusCode(final int statusCode) {
        this.statusCode = statusCode;
    }

    public void setStatusMsg(final String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public void setStudentId(final String studentId) {
        this.studentId = studentId;
    }

    public void setStudentName(final String studentName) {
        this.studentName = studentName;
    }

    public void setRoomId(final Long roomId) {
        this.roomId = roomId;
    }

    public void setRoomName(final String roomName) {
        this.roomName = roomName;
    }
}
