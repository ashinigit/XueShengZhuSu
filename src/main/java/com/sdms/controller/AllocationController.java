package com.sdms.controller;


import java.util.List;
import javax.annotation.Resource;

import com.sdms.common.page.Page;
import com.sdms.common.page.PageRequest;
import com.sdms.common.result.LayuiResult;
import com.sdms.entity.Student;
import com.sdms.service.RoomAllocationService;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static com.sdms.common.result.LayuiResult.ResultCode.FAILED;
import static com.sdms.common.result.LayuiResult.ResultCode.SUCCESS;
import static com.sdms.common.util.StringUtils.parseStringList;

@SuppressWarnings("unused")
@Controller
public class AllocationController {

    @Resource
    private RoomAllocationService allocationService;

    @GetMapping(value = {"/admin/allocation-list"})
    public String toAdminAllocationList() {
        return "admin/allocation-list"; // Thymeleaf模板的名字,表示 templates/admin/allocation-list.html
    }

    @Operation(summary = "ajax:分页查询学生住宿信息")
    @RequestMapping(value = "/admin/allocations", method = {RequestMethod.POST})
    @ResponseBody
    public Page<Student> fetchPage(@RequestBody PageRequest pageRequest) {
        return allocationService.fetchPage(pageRequest);
    }

    @Operation(summary = "ajax:根据若干学号为学生完成解约")
    @RequestMapping(value = "/admin/allocation/release", method = {RequestMethod.POST})
    @ResponseBody
    public LayuiResult<String> releaseStudentByIds(String ids) {
        List<String> idList = parseStringList(ids);
        if (allocationService.releaseStudentByIds(idList).isSuccess()) {
            return new LayuiResult<>(SUCCESS, null, null);
        } else {
            return new LayuiResult<>(FAILED, null, null);
        }
    }
}
