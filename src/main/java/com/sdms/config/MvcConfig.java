package com.sdms.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

import static com.sdms.common.util.StringUtils.isBlankOrNull;

@SuppressWarnings("unused")
@Configuration
public class MvcConfig {

    private static final Logger log = LoggerFactory.getLogger(MvcConfig.class);

    @Resource
    private PictureConfig pictureConfig;

    @Bean
    public WebMvcConfigurer webMvcConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                log.info("user.dir={}", System.getProperty("user.dir"));
                // 静态资源路径映射
                if (isBlankOrNull(pictureConfig.getPath())) {
                    registry.addResourceHandler("/sdms-images/**")
                            .addResourceLocations("file:" + System.getProperty("user.dir").replace("\\", "/") + "/picture-path/");
                } else {
                    // 将 http://localhost:8080/sdms-images/** 格式的 URL映射到 pictureConfig.getPath() 这个本地绝对路径下
                    registry.addResourceHandler("/sdms-images/**")
                            .addResourceLocations("file:" + pictureConfig.getPath());
                }
            }
        };
    }
}
