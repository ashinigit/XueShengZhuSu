package com.sdms.common.page;

import com.querydsl.core.QueryResults;
import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Collection;
import java.util.Collections;

/**
 * 为Layui设计的分页对象,本质上是一种 DTO,
 * 如果在controller层将Page作为方法返回值,
 * 最终会在Advice中被包装成LayuiResult对象返回给前端
 *
 * @param <E> 实体的类型
 */
@SuppressWarnings("unused")
@Schema(title = "分页")
public class Page<E> {

    /**
     * 实体在数据库中的总数
     */
    @Schema(title = "实体在数据库中的总数")
    long totalCount;

    /**
     * 当前分页包含的若干个实体
     */
    @Schema(title = "当前分页包含的若干实体")
    Collection<E> content;

    /**
     * @param result queryDSL调用fetchResults获得的查询结果
     * @param <E>    实体
     * @return 返回一个包含若干实体的分页
     */
    public static <E> Page<E> of(QueryResults<E> result) {
        Page<E> page = new Page<>();
        page.totalCount = result.getTotal();
        page.content = result.getResults();
        return page;
    }

    /**
     * @param <E> 实体
     * @return 返回一个空页
     */
    public static <E> Page<E> empty() {
        Page<E> page = new Page<>();
        page.totalCount = 0;
        page.content = Collections.emptyList();
        return page;
    }

    /**
     * 实体在数据库中的总数
     */
    public long getTotalCount() {
        return this.totalCount;
    }

    /**
     * 当前分页包含的若干个实体
     */
    public Collection<E> getContent() {
        return this.content;
    }

    /**
     * 实体在数据库中的总数
     */
    public void setTotalCount(final long totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * 当前分页包含的若干个实体
     */
    public void setContent(final Collection<E> content) {
        this.content = content;
    }

    public Page() {
    }

    public Page(final long totalCount, final Collection<E> content) {
        this.totalCount = totalCount;
        this.content = content;
    }
}