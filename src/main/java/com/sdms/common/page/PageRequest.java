package com.sdms.common.page;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.HashMap;
import java.util.Map;

/**
 * 为前端Layui的Table组件设计的分页请求结构
 */
@SuppressWarnings("unused")
@Schema(title = "分页请求")
public class PageRequest {

    @Schema(title = "页号,从1开始计数")
    private long page = 1;

    @Schema(title = "每一页的容量")
    private long limit = 10;

    @Schema(title = "0表示模糊查询,其它表示精确查询")
    private int queryType = 0;

    @Schema(title = "其它查询参数")
    private Map<String, String> param = new HashMap<>();

    public long getPage() {
        return this.page;
    }

    public long getLimit() {
        return this.limit;
    }

    public int getQueryType() {
        return this.queryType;
    }

    public Map<String, String> getParam() {
        return this.param;
    }

    public void setPage(final long page) {
        this.page = page;
    }

    public void setLimit(final long limit) {
        this.limit = limit;
    }

    public void setQueryType(final int queryType) {
        this.queryType = queryType;
    }

    public void setParam(final Map<String, String> param) {
        this.param = param;
    }

    public PageRequest() {
    }

    public PageRequest(final long page, final long limit, final int queryType, final Map<String, String> param) {
        this.page = page;
        this.limit = limit;
        this.queryType = queryType;
        this.param = param;
    }
}