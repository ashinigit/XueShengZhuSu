package com.sdms.common.result;

/**
 * OperationResult的本质是一种 DTO ,
 * service层执行写操作(包括 增、删、改)的方法统一返回OperationResult对象
 */
public class OperationResult<T> {
    private boolean success = false;
    private String msg = "";
    private T value = null;

    private static <T> OperationResult<T> newInstance(boolean success, T value, String msg) {
        OperationResult<T> result = new OperationResult<>();
        result.success = success;
        result.value = value;
        result.msg = msg;
        return result;
    }

    public static <T> OperationResult<T> success(T value) {
        return newInstance(true, value, "");
    }

    public static <T> OperationResult<T> failure(String msg) {
        return newInstance(false, null, msg);
    }

    public void setSuccess(final boolean success) {
        this.success = success;
    }

    public void setMsg(final String msg) {
        this.msg = msg;
    }

    public void setValue(final T value) {
        this.value = value;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public String getMsg() {
        return this.msg;
    }

    public T getValue() {
        return this.value;
    }
}
