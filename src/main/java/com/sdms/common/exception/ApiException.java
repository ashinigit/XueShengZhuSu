package com.sdms.common.exception;

import com.sdms.common.result.LayuiResult;

public class ApiException extends RuntimeException {
    private final int code;
    private final String msg;

    public ApiException() {
        this(LayuiResult.ResultCode.FAILED);
    }

    public ApiException(LayuiResult.ResultCode failed) {
        this.code = failed.getCode();
        this.msg = failed.getMsg();
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }
}