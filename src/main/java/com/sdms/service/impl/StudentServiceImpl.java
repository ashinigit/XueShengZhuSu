package com.sdms.service.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sdms.common.page.Page;
import com.sdms.common.page.PageRequest;
import com.sdms.common.result.OperationResult;
import com.sdms.common.util.MD5Utils;
import com.sdms.dao.StudentDao;
import com.sdms.dao.UserDao;
import com.sdms.entity.*;
import com.sdms.qmodel.QRoom;
import com.sdms.qmodel.QStudent;
import com.sdms.qmodel.QTeachingClass;
import com.sdms.qmodel.QUser;
import com.sdms.service.StudentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.sdms.common.result.OperationResult.failure;
import static com.sdms.common.result.OperationResult.success;
import static com.sdms.common.util.StringUtils.*;

@SuppressWarnings("unused")
@Service
public class StudentServiceImpl implements StudentService {

    private static final Logger log = LoggerFactory.getLogger(StudentServiceImpl.class);

    @Resource
    private StudentDao studentDao;

    @Resource
    private UserDao userDao;

    @Resource
    private JPAQueryFactory queryFactory;

    @Override
    public void fillTransientFields(Student student) {
        User user = student.getUser();
        if (user != null) {
            student.setUserId(user.getId());
            student.setUsername(user.getUsername());
            student.setPhone(user.getPhone());
        }
        Room room = student.getRoom();
        if (room != null) {
            student.setRoomId(room.getId());
            student.setRoomName(room.getName());
        }
        TeachingClass teachingClass = student.getTeachingClass();
        if (teachingClass != null) {
            student.setTeachingClassId(teachingClass.getId());
            student.setTeachingClassName(teachingClass.getName());
        }
    }

    @Override
    public Page<Student> fetchPage(PageRequest request) {
        boolean vague = request.getQueryType() == 0; // 0 表示模糊查询
        Map<String, String> param = request.getParam();
        String studentId = trimAllWhitespace(param.get("studentId"));
        Long teachingClassId = parseLong(param.get("teachingClassId"));
        String keyWord = trimAllWhitespace(param.get("keyWord"));
        QStudent student = QStudent.student;
        QTeachingClass teachingClass = QTeachingClass.teachingClass;
        QUser user = QUser.user;
        // 动态拼接查询条件
        BooleanBuilder condition = new BooleanBuilder();
        if (teachingClassId != null) {
            condition.and(teachingClass.id.eq(teachingClassId));
        }
        BooleanBuilder subCondition = new BooleanBuilder();
        if (vague) {
            // 模糊查询
            if (!isBlankOrNull(keyWord)) {
                subCondition.or(student.name.like("%" + keyWord + "%"));
            }
            if (!isBlankOrNull(studentId)) {
                subCondition.or(student.id.like("%" + studentId + "%"));
            }
        } else {
            // 精确查询
            if (!isBlankOrNull(keyWord)) {
                subCondition.or(student.name.eq(keyWord));
            }
            if (!isBlankOrNull(studentId)) {
                subCondition.or(student.id.eq(studentId));
            }
        }
        condition.and(subCondition);
        QRoom room = QRoom.room;
        JPAQuery<Student> query = queryFactory
                .select(Projections.bean(Student.class,
                        student.id,
                        student.name,
                        room.id.as("roomId"),
                        room.name.as("roomName"),
                        user.username.as("username"),
                        teachingClass.id.as("teachingClassId"),
                        teachingClass.name.as("teachingClassName")))
                .from(student)
                .leftJoin(room).on(student.room.id.eq(room.id))
                .leftJoin(teachingClass).on(student.teachingClass.id.eq(teachingClass.id))
                .leftJoin(user).on(student.user.id.eq(user.id))
                .orderBy(student.id.asc())
                .where(condition)
                .offset((request.getPage() - 1) * request.getLimit()).limit(request.getLimit());
        // 执行分页查询
        QueryResults<Student> result = query.fetchResults();
        result.getResults().forEach(this::fillTransientFields);
        return Page.of(result);
    }

    @Override
    public Student getStudentById(String id) {
        Optional<Student> optional = studentDao.findById(id);
        if (optional.isPresent()) {
            Student student = optional.get();
            this.fillTransientFields(student);
            return student;
        }
        return null;
    }

    @Override
    public OperationResult<Student> saveStudent(Student student) {
        if (student == null || isBlankOrNull(student.getId())) {
            return failure("学生为null");
        }
        String name = student.getName();
        if (isBlankOrNull(name)) {
            return failure("学生姓名不可以为空或者空白串");
        }
        if (!studentDao.existsById(student.getId())) {
            // 新增,同时创建一个用户名和密码都是学号的用户
            User user = new User();
            user.setUsername(student.getId());
            user.setPassword(student.getId());
            Role role = new Role();
            role.setId(Role.STUDENT_ROLE_ID); // 1表示学生
            user.setRole(role);
            userDao.save(user);
            if (user.getId() == null && userDao.findByUsername(user.getUsername()) != null) {
                return failure("新用户的账号与其它用户重复");
            }
            String rawPassword = user.getPassword();
            if (isBlankOrNull(rawPassword)) {
                return failure("密码不可以为空或者空白串");
            }
            // 性别转换
            user.setGender(User.Gender.of(user.getGenderStr()));
            // 密码加密
            user.setPassword(MD5Utils.encodeWithSalt(rawPassword, user.getUsername(), 1));
            student.setUser(userDao.save(user));
        }
        return success(studentDao.save(student));
    }

    @Override
    public OperationResult<String> deleteStudentByIds(Collection<String> ids) {
        try {
            studentDao.deleteStudentsByIdIn(ids);
        } catch (Exception e) {
            log.error("学生删除失败,ids={},error is {}", ids, e.getMessage());
            return failure("删除失败");
        }
        return success("删除成功");
    }

    @Override
    public List<Student> listAllStudents() {
        return studentDao.findAll();
    }

    @Override
    public Student getStudentByUserId(Long id) {
        return studentDao.getStudentByUserId(id);
    }
}
