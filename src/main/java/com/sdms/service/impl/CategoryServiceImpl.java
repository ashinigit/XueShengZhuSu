package com.sdms.service.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sdms.common.page.Page;
import com.sdms.common.page.PageRequest;
import com.sdms.common.result.OperationResult;
import com.sdms.dao.CategoryDao;
import com.sdms.entity.Category;
import com.sdms.qmodel.QCategory;
import com.sdms.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.sdms.common.result.OperationResult.failure;
import static com.sdms.common.result.OperationResult.success;
import static com.sdms.common.util.StringUtils.isBlankOrNull;
import static com.sdms.common.util.StringUtils.parseLong;
import static com.sdms.common.util.StringUtils.trimAllWhitespace;

@SuppressWarnings("unused")
@Service
public class CategoryServiceImpl implements CategoryService {

    private static final Logger log = LoggerFactory.getLogger(CategoryServiceImpl.class);

    @Resource
    private CategoryDao categoryDao;

    @Resource
    private JPAQueryFactory queryFactory;

    @Override
    public Page<Category> fetchPage(PageRequest request) {
        boolean vague = request.getQueryType() == 0; // 0 表示模糊查询
        Map<String, String> param = request.getParam();
        Long categoryId = parseLong(param.get("categoryId"));
        String categoryName = trimAllWhitespace(param.get("keyWord"));
        QCategory category = QCategory.category;
        // 动态拼接查询条件
        BooleanBuilder condition = new BooleanBuilder();
        if (categoryId != null) {
            condition.or(category.id.eq(categoryId));
        }
        if (!isBlankOrNull(categoryName)) {
            if (vague) {
                // 模糊查询
                condition.or(category.name.like(String.format("%%%s%%", categoryName)));
            } else {
                // 精确查询
                condition.or(category.name.eq(categoryName));
            }
        }
        JPAQuery<Category> query = queryFactory
                .select(Projections.bean(Category.class,
                        category.id,
                        category.name,
                        category.maxSize))
                .from(category)
                .orderBy(category.id.asc())
                .where(condition)
                .offset((request.getPage() - 1) * request.getLimit()).limit(request.getLimit());
        // 执行分页查询
        QueryResults<Category> result = query.fetchResults();
        return Page.of(result);
    }

    @Override
    public Category getCategoryById(Long id) {
        Optional<Category> optional = categoryDao.findById(id);
        if (optional.isPresent()) {
            Category category = optional.get();
            this.fillTransientFields(category);
            return category;
        }
        return null;
    }

    @Override
    public OperationResult<Category> saveCategory(Category category) {
        if (category == null) {
            return failure("寝室类型为null");
        }
        String name = category.getName();
        if (isBlankOrNull(name)) {
            return failure("寝室类型名称不可以为空或者空白串");
        }
        return success(categoryDao.save(category));
    }

    @Override
    public OperationResult<String> deleteCategoryByIds(Collection<Long> ids) {
        try {
            categoryDao.deleteCategoriesByIdIn(ids);
        } catch (Exception e) {
            log.error("寝室类型删除失败,ids={},error is {}", ids, e.getMessage());
            return failure("删除失败");
        }
        return success("删除成功");
    }

    @Override
    public List<Category> listAllCategories() {
        return categoryDao.findAll();
    }
}
