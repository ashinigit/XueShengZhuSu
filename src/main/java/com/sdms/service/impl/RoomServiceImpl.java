package com.sdms.service.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.core.Tuple;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sdms.common.page.Page;
import com.sdms.common.page.PageRequest;
import com.sdms.common.result.OperationResult;
import com.sdms.dao.RoomDao;
import com.sdms.entity.Category;
import com.sdms.entity.Room;
import com.sdms.entity.User;
import com.sdms.qmodel.QCategory;
import com.sdms.qmodel.QRoom;
import com.sdms.qmodel.QStudent;
import com.sdms.qmodel.QUser;
import com.sdms.service.RoomService;
import com.sdms.service.SessionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

import static com.sdms.common.result.OperationResult.failure;
import static com.sdms.common.result.OperationResult.success;
import static com.sdms.common.util.StringUtils.*;

@SuppressWarnings("unused")
@Service
public class RoomServiceImpl implements RoomService {

    private static final Logger log = LoggerFactory.getLogger(RoomServiceImpl.class);

    @Resource
    private RoomDao roomDao;

    @Resource
    private SessionService sessionService;

    @Resource
    private JPAQueryFactory queryFactory;

    @Override
    public void fillTransientFields(Room room) {
        Category category = room.getCategory();
        if (category != null) {
            room.setCategoryId(category.getId());
            room.setCategoryName(category.getName());
        }
    }

    @Override
    public Page<Room> fetchPage(PageRequest request) {
        boolean vague = request.getQueryType() == 0; // 0 表示模糊查询
        Map<String, String> param = request.getParam();
        Long roomId = parseLong(param.get("roomId"));
        Long categoryId = parseLong(param.get("categoryId"));
        String keyWord = trimAllWhitespace(param.get("keyWord"));
        QRoom room = QRoom.room;
        QCategory category = QCategory.category;
        // 动态拼接查询条件
        BooleanBuilder condition = new BooleanBuilder();
        if (categoryId != null) {
            condition.and(category.id.eq(categoryId));
        }
        BooleanBuilder subCondition = new BooleanBuilder();
        if (roomId != null) {
            subCondition.or(room.id.eq(roomId));
        }
        if (!isBlankOrNull(keyWord)) {
            if (vague) {
                // 模糊查询
                subCondition.or(room.name.like("%" + keyWord + "%"));
                subCondition.or(room.address.like("%" + keyWord + "%"));
            } else {
                // 精确查询
                subCondition.or(room.name.eq(keyWord));
                subCondition.or(room.address.eq(keyWord));
            }
        }
        condition.and(subCondition);
        JPAQuery<Room> query = queryFactory
                .select(Projections.bean(Room.class,
                        room.id,
                        room.name,
                        room.address,
                        room.picture,
                        category.id.as("categoryId"),
                        category.name.as("categoryName")))
                .from(room).leftJoin(category).on(room.category.id.eq(category.id))
                .orderBy(room.id.asc())
                .where(condition)
                .offset((request.getPage() - 1) * request.getLimit()).limit(request.getLimit());
        // 执行分页查询
        QueryResults<Room> result = query.fetchResults();
        List<Long> roomIdList = new ArrayList<>();
        result.getResults().forEach(r -> roomIdList.add(r.getId()));
        QStudent student = QStudent.student;
        final List<Tuple> tupleList = queryFactory
                .select(student.id, student.name, room.id)
                .from(student).leftJoin(room).on(student.room.id.eq(room.id))
                .where(student.room.id.in(roomIdList))
                .fetch();
        for (Room r : result.getResults()) {
            fillStudentsNameAndId(r, room, student, tupleList);
        }
        return Page.of(result);
    }

    @Override
    public Room getRoomById(Long id) {
        Optional<Room> optional = roomDao.findById(id);
        if (optional.isPresent()) {
            Room room = optional.get();
            this.fillTransientFields(room);
            return room;
        }
        return null;
    }

    @Override
    public OperationResult<Room> saveRoom(Room room) {
        if (room == null) {
            return failure("寝室为null");
        }
        String name = room.getName();
        if (isBlankOrNull(name)) {
            return failure("寝室名称不可以为空或者空白串");
        }
        return success(roomDao.save(room));
    }

    @Override
    public OperationResult<String> deleteRoomByIds(Collection<Long> ids) {
        try {
            roomDao.deleteRoomsByIdIn(ids);
        } catch (Exception e) {
            log.error("寝室删除失败,ids={},error is {}", ids, e.getMessage());
            return failure("删除失败");
        }
        return success("删除成功");
    }

    @Override
    public List<Room> listAllRooms() {
        return roomDao.findAll();
    }

    @Override
    public Room getCurrentStudentRoom() {
        User currentUser = sessionService.getCurrentUser();
        if (currentUser == null || currentUser.getId() == null) {
            return null;
        }
        QRoom room = QRoom.room;
        QStudent student = QStudent.student;
        QUser user = QUser.user;
        return queryFactory.selectFrom(room)
                .rightJoin(student).on(student.room.id.eq(room.id))
                .rightJoin(user).on(student.user.id.eq(user.id))
                .where(user.id.eq(currentUser.getId())).fetchOne();
    }

    @Override
    public long countStudentsByRoomId(Long id) {
        QStudent student = QStudent.student;
        return queryFactory.selectFrom(student).where(student.room.id.eq(id)).fetchCount();
    }

    @Override
    public Room getRoomWithStudentsById(Long id) {
        Room obj = getRoomById(id);
        if (obj == null) {
            return null;
        }
        QRoom room = QRoom.room;
        QStudent student = QStudent.student;
        final List<Tuple> tupleList = queryFactory
                .select(student.id, student.name, room.id)
                .from(student).leftJoin(room).on(student.room.id.eq(room.id))
                .where(room.id.eq(obj.getId()))
                .fetch();
        fillStudentsNameAndId(obj, room, student, tupleList);
        return obj;
    }

    private void fillStudentsNameAndId(Room obj, QRoom room, QStudent student, List<Tuple> tupleList) {
        List<String> stringList = new ArrayList<>();
        for (Tuple tuple : tupleList) {
            if (Objects.equals(tuple.get(room.id), obj.getId())) {
                stringList.add(tuple.get(student.name) + "|" + tuple.get(student.id));
            }
        }
        obj.setStudentsNameAndId(String.join(",", stringList));
    }
}
