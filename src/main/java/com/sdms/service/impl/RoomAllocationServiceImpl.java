package com.sdms.service.impl;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sdms.common.page.Page;
import com.sdms.common.page.PageRequest;
import com.sdms.common.result.OperationResult;
import com.sdms.dao.StudentDao;
import com.sdms.entity.Student;
import com.sdms.qmodel.QRoom;
import com.sdms.qmodel.QStudent;
import com.sdms.qmodel.QTeachingClass;
import com.sdms.qmodel.QUser;
import com.sdms.service.RoomAllocationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

import static com.sdms.common.result.OperationResult.failure;
import static com.sdms.common.result.OperationResult.success;
import static com.sdms.common.util.StringUtils.isBlankOrNull;
import static com.sdms.common.util.StringUtils.trimAllWhitespace;

@SuppressWarnings("unused")
@Service
public class RoomAllocationServiceImpl implements RoomAllocationService {

    private static final Logger log = LoggerFactory.getLogger(RoomAllocationServiceImpl.class);

    @Resource
    private StudentDao studentDao;

    @Resource
    private JPAQueryFactory queryFactory;

    @Override
    public Page<Student> fetchPage(PageRequest request) {
        String keyWord = trimAllWhitespace(request.getParam().get("keyWord"));
        QStudent student = QStudent.student;
        QRoom room = QRoom.room;
        QTeachingClass teachingClass = QTeachingClass.teachingClass;
        QUser user = QUser.user;
        // 动态拼接查询条件
        BooleanBuilder condition = new BooleanBuilder();
        switch (request.getQueryType()) {
            case 0: // 按学生姓名查找
                if (!isBlankOrNull(keyWord)) {
                    condition.and(student.name.like("%" + keyWord + "%"));
                }
                break;
            case 1: // 按寝室查找
                if (!isBlankOrNull(keyWord)) {
                    condition.and(room.name.like("%" + keyWord + "%"));
                }
                break;
            case 2: // 按班级查找
                if (!isBlankOrNull(keyWord)) {
                    condition.and(teachingClass.name.like("%" + keyWord + "%"));
                }
                break;
            default:
                break;
        }
        condition.and(student.room.isNotNull());
        JPAQuery<Student> query = queryFactory
                .select(Projections.bean(Student.class,
                        student.id,
                        student.name,
                        teachingClass.id.as("teachingClassId"),
                        teachingClass.name.as("teachingClassName"),
                        room.id.as("roomId"),
                        room.name.as("roomName"),
                        user.phone.as("phone")))
                .from(student)
                .leftJoin(room).on(student.room.id.eq(room.id))
                .leftJoin(teachingClass).on(student.teachingClass.id.eq(teachingClass.id))
                .leftJoin(user).on(student.user.id.eq(user.id))
                .orderBy(student.id.asc())
                .where(condition)
                .offset((request.getPage() - 1) * request.getLimit()).limit(request.getLimit());
        // 执行分页查询
        QueryResults<Student> result = query.fetchResults();
        return Page.of(result);
    }

    @Override
    public OperationResult<String> releaseStudentByIds(Collection<String> ids) {
        try {
            List<Student> studentList = studentDao.findAllById(ids);
            for (Student student : studentList) {
                student.setRoom(null);
            }
            studentDao.saveAll(studentList);
        } catch (Exception e) {
            log.error("解约失败,ids={} error:{}", ids, e.getMessage());
            return failure("解约失败");
        }
        return success("解约成功");
    }
}
