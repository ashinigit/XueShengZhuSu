package com.sdms.dao;

import com.sdms.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

@SuppressWarnings("unused")
public interface PermissionDao extends JpaRepository<Permission, Long> {
}
