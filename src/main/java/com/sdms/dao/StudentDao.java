package com.sdms.dao;

import com.sdms.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Collection;

public interface StudentDao extends JpaRepository<Student, String>, JpaSpecificationExecutor<Student> {

    @Modifying
    @Transactional
    void deleteStudentsByIdIn(Collection<String> ids);

    @Query(value = "select * from t_student where user_id = :userId", nativeQuery = true)
    Student getStudentByUserId(@Param("userId") Long id);
}
